FROM ubuntu:18.04

RUN apt-get update -y && \
    apt-get install --no-install-recommends -y python3-pip python-dev 
        
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "app.py" ]
